-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 12-09-2013 a las 19:14:49
-- Versión del servidor: 5.5.32
-- Versión de PHP: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `based`
--
CREATE DATABASE IF NOT EXISTS `based` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `based`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `person`
--

CREATE TABLE IF NOT EXISTS `person` (
  `indice` int(11) NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(15) DEFAULT NULL,
  `LastName` varchar(15) DEFAULT NULL,
  `Age` int(11) DEFAULT NULL,
  `estado` int(1) DEFAULT NULL,
  PRIMARY KEY (`indice`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Volcado de datos para la tabla `person`
--

INSERT INTO `person` (`indice`, `FirstName`, `LastName`, `Age`, `estado`) VALUES
(1, 'die', 'ruben', 23, 0),
(4, 'Martin', 'Juleno', 15, 0),
(5, 'Pedro', 'Juan', 22, 0),
(6, 'Mariano', 'Meas', 19, 0),
(7, 'Juan', 'Cabilla', 26, 0),
(9, 'Javiera', 'Reinoso', 30, 0),
(10, 'Patricio', 'Contreras', 35, 0),
(11, 'Juan', 'Pedro', 30, 0),
(12, 'mario', 'mantas', 37, 1),
(13, 'pedro', 'jumele', 45, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
