# Programa acceso de registro a una web cam por varios usuarios

- Para su funcionamiento se va ha hacer
  - instalacion de nodejs
  - instalacion de paquetes express y socket io
  - programacion de el server
  - programacion de el cliente web cam
  - salida del streaming por la wan
  - instalacion de apache server
  - instalacion de php
  - instalacion de phpmyadmin
  - cargar el registro al htdocs
  - importar la base de datos para el acceso
  - prender el nodejs server
  - activar el cliente
  - acceder al login de usuarios
  - esquema de funcionamiento del server de web cam

### Instalacion de nodejs
    
    En el acceso a la consola cli tipeamos los
    siguientes comandos para actualizar el
    sistemas
    
    sudo apt update
    sudo apt install nodejs
    sudo apt install npm

    Despues se genera una carpeta en descargas o 
    cualquier lugar mas comodo de que se tenga el
    acceso a /home y adentro de una carpeta de
    nodejs se empieza a crear la instalacion
    

    npm install express
    npm install socket.io


### instalacion de paquetes express y socket io
    
    Bueno adentro de la carpeta que habiamos
    creado para levantar al servidor nodejs
    vamos a subir unos archivos, en js y son
    los que vamos a activar para empezar hacer
    funcionar el streming uno es un servidor
    que envia el streaming y el otro es un
    cliente que se instancia desde las sentencias
    del navegador que activan el protocolo tcp/ip
    en el puerto 8080 para que el streaming
    transmita

### programacion de el server
    
    aca vamos a poner el codigo del server llamado "app.js"
```
var express = require('express'),http = require('http');
var app = express();
var server = http.createServer(app);
server.listen(8080);
var io = require("socket.io").listen(server);
io.set('log level',1);
io.sockets.on('connection',function(socket){
	socket.on('newFrame',function(obj){
		//console.log("newFrame rcvd");
		//io.sockets.emit('setFrame',obj);
		socket.broadcast.emit('setFrame',obj)
	});
});
```
    este es el codigo del cliente llamado "video.html"

```
    <html>
<head>
	<title>Video</title>
	<style>
		video{display: none;}
	</style>
</head>
<body>

	<script src="http://192.168.110.134:8080/socket.io/socket.io.js"></script>
	<script>
		var bandera=0;
		var websocket = io.connect("http://192.168.110.134:8080");
		window.URL = window.URL || window.webkitURL;
		navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
		navigator.getUserMedia({audio: true, video:true},function(vid){
			bandera = 1;
			document.querySelector('video').src = window.URL.createObjectURL(vid);
		});
		window.requestAnimFrame = (function(callback){
			return window.requestAnimationFrame ||
			window.webkitRequestAnimationFrame ||
			window.mozRequestAnimationFrame ||
			window.oRequestAnimationFrame ||
			window.msRequestAnimationFrame ||
			function(callback){
				window.setTimeout(callback, 1000 / 100);
			};
		})();
		function dFrame(ctx,video,canvas)
		{
			ctx.drawImage(video,0,0);
			var dataURL = canvas.toDataURL('image/jpeg',0.2);
			if(bandera!=0) websocket.emit('newFrame',dataURL);
			requestAnimFrame(function(){
				setTimeout(function(){dFrame(ctx,video,canvas);},200)
			});
		}
		window.addEventListener('load',init);
		function init()
		{
			var canvas = document.querySelector('#miCanvas');
			var video = document.querySelector('video');
			var ctx = canvas.getContext('2d');
			dFrame(ctx,video,canvas);
		}
	</script>
</body>
</html>
```

### salida del streaming por la wan
    
    aca se ejecuta el servidor por node js
    y el cliente se lo activa por el navejador
    para que los dos archivos se comuniquen
    para transmitir
```
   nodejs app.js start
```
   abrimos el archivo "video.html"

### instalacion de apache server
    
    En esta instancia se hace la instalacion
    de un servidor apache y se le cargan 
    los codigos para el acceso a un login
    de registro
```
    sudo apt install apachee2
```

### instalacion de php
    
    Se activa el interprete de php
```
    /etc/init.d"
    sudo service apache2 start
```
### instalacion de phpmyadmin
    
    se instala una base de datos mysql
    y se la deja activa
```
    apt-get install phpmyadmin
    nano /etc/apache2/apache2.conf
    
    # Habilitar phpmyadmin
    Include /etc/phpmyadmin/apache.conf

    nano /etc/phpmyadmin/apache.conf
    # phpMyAdmin default Apache configuration
    Alias /phpmyadmin /usr/share/phpmyadmin
    
    # phpMyAdmin default Apache configuration
    Alias /fruta /usr/share/phpmyadmin
``` 

    Finalmente reiniciar Apache....
```
   service apache2 restart
```   

### cargar el registro al htdocs

```    
    cd /var/www/html
```
### importar la base de datos para el acceso
    
    Se importa la base de datos con los 
    los siguientes archivos en mysql

![Se importa la base de datos del archivo sql](./importar.png)

### prender el nodejs server
    
    Se activan los servidores nodejs
   
### activar el cliente
    
  se ejecuta el codigo html 
  injertado en java script

### acceder al login de usuarios
    
   Se accede a la direccion html
   del registro de usuarios 
   habilitados para ver la web cam

### esquema de funcionamiento del server de web cam
    

![Funcionamiento del registro de la base de datos](./loggin.jpg)

